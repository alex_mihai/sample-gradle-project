package com.example;

import com.example.ui.CalculatorUi;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorUiTest {
    private CalculatorUi calculatorUi;

    private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        calculatorUi = new CalculatorUi();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void testWorkingOperation() {
        provideInput("+\n5\n3\n");
        calculatorUi.run();
        assertTrue(outputStream.toString().contains("Result: 8.0"));
    }

    @Test
    public void testInvalidOperation() {
        provideInput("unknown\n5\n3\n");
        calculatorUi.run();
        assertTrue(outputStream.toString().contains("The given operator is not valid."));
    }

    private void provideInput(String input) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);
    }
}