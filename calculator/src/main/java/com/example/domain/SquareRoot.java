package com.example.domain;

public class SquareRoot implements IOperation {
    @Override
    public double calculator(double a, double b) {
        // the b parameter won't be used but the same method will be kept so that "IOperation" can be used
        return Math.sqrt(a);
    }
}
