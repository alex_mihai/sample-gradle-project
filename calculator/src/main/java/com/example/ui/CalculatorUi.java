package com.example.ui;

import com.example.service.CalculatorService;

import java.util.Scanner;

public class CalculatorUi {
    private final CalculatorService calculatorService;

    public CalculatorUi() {
        calculatorService = new CalculatorService();
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter one of the operators: +, -, *, /, min, max, sqrt: ");
        String operator = scanner.next();

        System.out.print("Enter the first number: ");
        double a = scanner.nextDouble();

        if (!operator.equals("sqrt")) {
            System.out.print("Enter the second number: ");
        }
        double b = operator.equals("sqrt") ? -1 : scanner.nextDouble();

        try {
            double result = calculatorService.calculator(operator, a, b);
            System.out.println("Result: " + result);
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
