package com.example;

import com.example.ui.CalculatorUi;

public class Main {
    public static void main(String[] args) {
        CalculatorUi calculatorUi = new CalculatorUi();
        calculatorUi.run();
    }
}
