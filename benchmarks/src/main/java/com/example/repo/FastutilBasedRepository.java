package com.example.repo;

import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;

import java.util.Collection;

public class FastutilBasedRepository<T> implements InMemoryRepository<T> {
    private final ObjectOpenHashSet<T> set;

    public FastutilBasedRepository() {
        set = new ObjectOpenHashSet<>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        set.addAll(objects);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
