package com.example.repo;

import gnu.trove.set.hash.THashSet;

import java.util.Collection;

public class Trove4jBasedRepository<T> implements InMemoryRepository<T> {
    private THashSet<T> set;

    public Trove4jBasedRepository() {
        set = new THashSet<>();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        set.addAll(objects);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
