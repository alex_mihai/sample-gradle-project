package com.example.repo;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private ConcurrentMap<T, Boolean> map;

    public ConcurrentHashMapBasedRepository() {
        map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T item) {
        map.put(item, true);
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
    }

    @Override
    public void remove(T item) {
        map.remove(item);
    }
}

