package com.example.repo;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.factory.Sets;

import java.util.Collection;

public class EclipseCollectionsBasedRepository<T> implements InMemoryRepository<T> {
    private MutableSet<T> set;

    public EclipseCollectionsBasedRepository() {
        set = Sets.mutable.empty();
    }

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public void addAll(Collection<T> objects) {
        set.addAll(objects);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
